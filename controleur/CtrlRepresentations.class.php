<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace controleur;

use controleur\GestionErreurs;
use modele\dao\DaoRepresentation;
use modele\metier\Representation;
use modele\dao\DaoLieu;
use modele\dao\GroupeDAO;
use modele\metier\Groupe;
use modele\metier\Lieu;
use modele\dao\Bdd;
use vue\representations\VueListeRepresentations;
use vue\representations\VueSaisieRepresentations;
use vue\representations\VueSupprRep;


class CtrlRepresentations extends ControleurGenerique {

    /** controleur= representations & action= defaut
     * Afficher la liste des representations     */
    public function defaut() {
        $this->liste();
    }

    /** controleur= representations & action= liste
     * Afficher la liste des representations      */
    public function liste() {
        $laVue = new VueListeRepresentations();
        $this->vue = $laVue;
        // On récupère un tableau composé de la liste des representations 
        Bdd::connecter();
        $laVue->setLesRepresentations($this->getTabRepresentations());
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - Representations");
        $this->vue->afficher();
    }

    /** controleur= representations & action=creer
     * Afficher le formulaire d'ajout d'une representation     */
    public function creer() {
        $laVue = new VueSaisieRepresentations();
        $this->vue = $laVue;
        $laVue->setActionRecue("creer");
        $laVue->setActionAEnvoyer("validerCreer");
        $laVue->setMessage("Nouvelle représentation");
        // En création, on affiche un formulaire vide
        /* @var Representation $uneRepr */
        $unGroupe = new Groupe("", "", "", "", "", "", "");
        $unLieu = new Lieu("", "", "", "");
        $uneRepr = new Representation("", $unLieu, $unGroupe, "", "", "");
        $laVue->setUneRepresentation($uneRepr);
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - représentations");
        $this->vue->afficher();
    }

    /** controleur= representations & action=validerCreer
     * ajout d'une representation dans la base de données d'après la saisie    */
    public function validerCreer() {
        Bdd::connecter();
        /* @var Représentation $uneRepr  : récupération du contenu du formulaire et instanciation d'une représentation */
        $uneRepr = new Representation($_REQUEST['id_representation'], $_REQUEST['id_lieu'], $_REQUEST['id_groupe'], $_REQUEST['date'], $_REQUEST['heure_debut'], $_REQUEST['heure_fin']);
        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de création (paramètre n°1 = true)
        $this->verifierDonneesEtab($uneRepr, true);
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer la représentation
            DaoRepresentation::insert($uneRepr);
            // revenir à la liste des représentations
            header("Location: index.php?controleur=representations&action=liste");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de création
            $laVue = new VueSaisieRepresentations();
            $this->vue = $laVue;
            $laVue->setActionRecue("creer");
            $laVue->setActionAEnvoyer("validerCreer");
            $laVue->setMessage("Nouvelle représentation");
            $laVue->setUneRepresentationt($uneRepr);
            parent::controlerVueAutorisee();
            $laVue->setTitre("Festival - represéntations");
            $this->vue->afficher();
        }
    }

    /** controleur= representations & action=modifier $ id=identifiant de l'établissement à modifier
     * Afficher le formulaire de modification d'une representation     */
    public function modifier() {
        $idRepr = $_GET["id"];
        $laVue = new VueSaisieRepresentations();
        $this->vue = $laVue;
        // Lire dans la BDD les données de la représentation à modifier
        Bdd::connecter();
        /* @var Représentation $laRepresentation */
        $laRepresentation = DaoRepresentation::getOneById($idRepr);
        $this->vue->setUneRepresentation($laRepresentation);
        $laVue->setActionRecue("modifier");
        $laVue->setActionAEnvoyer("validerModifier");
        $laVue->setMessage("Modifier la représentation n°" . $laRepresentation->getId());
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - représentations");
        $this->vue->afficher();
    }

    /** controleur= representations & action=validerModifier
     * modifier une representation dans la base de données d'après la saisie    */
    public function validerModifier() {
        Bdd::connecter();
        /* @var Etablissement $unEtab  : récupération du contenu du formulaire et instanciation d'un établissement */
        $uneRepr = new Representation($_REQUEST['id_representation'], $_REQUEST['id_groupe'], $_REQUEST['id_lieu'], $_REQUEST['date'], $_REQUEST['heure_debut'], $_REQUEST['heure_fin']);

        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de modification (paramètre n°1 = false)
        $this->verifierDonneesEtab($uneRepr, false);
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer les modifications pour l'établissement
            DaoRepresentation::update($uneRepr->getId(), $uneRepr);
            // revenir à la liste des établissements
            header("Location: index.php?controleur=etablissements&action=liste");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de modification
            $laVue = new VueSaisieRepresentations();
            $this->vue = $laVue;
            $laVue->setUneRepresentation($uneRepr);
            $laVue->setActionRecue("modifier");
            $laVue->setActionAEnvoyer("validerModifier");
            $laVue->setMessage("Modifier la représentation n°" . $uneRepr->getId());
            parent::controlerVueAutorisee();
            $laVue->setTitre("Festival - représentations");
            $this->vue->afficher();
        }
    }

    /** controleur= representations & action=supprimer & id=id_representation
     * Supprimer une representation d'après son identifiant     */
    public function supprimer() {
        $idRep = $_GET["id"];
        $this->vue = new VueSupprRep();
        // Lire dans la BDD les données de la representation à supprimer
        Bdd::connecter();
        $this->vue->setUneRepresentation(DaoRepresentation::getOneById($idRep));
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - représentations");
        $this->vue->afficher();
    }//Polo was here
    

    /** controleur= representations & action= validerSupprimer
     * supprimer une representation dans la base de données après confirmation   */
    public function validerSupprimer() {
        Bdd::connecter();
        if (!isset($_GET["id_representation"])) {
            // pas d'identifiant fourni
            GestionErreurs::ajouter("Il manque l'identifiant de la représentation à supprimer");
        } else {
            // suppression de la représentation d'après son identifiant
            DaoRepresentation::delete($_GET["id_representation"]);
        }
        // retour à la liste des représentations
        header("Location: index.php?controleur=representations&action=liste");
    }

    /**
     * Vérification des données du formulaire de saisie
     * @param Representation $uneRepr représentation à vérifier
     * @param bool $creation : =true si formulaire de création d'un nouvelle représentation ; =false sinon
     */
    private function verifierDonneesRepr(Representation $uneRepr, bool $creation) {
        // Vérification des champs obligatoires.
        // Dans le cas d'une création, on vérifie aussi l'id
        if (($creation && $uneRepr->getId() == "") || $uneRepr->getLeLieu() == "" || $uneRepr->getLeGroupe() == "" || $uneRepr->getDateRep() == "" ||
                $uneRepr->getHeureDebut() == "" || $uneRepr->getHeureFin() == "") {
            GestionErreurs::ajouter('Chaque champ suivi du caractère * est obligatoire');
        }
        // En cas de création, vérification du format de l'id et de sa non existence
        if ($creation && $uneRepr->getId() != "") {
            // Si l'id est constitué d'autres caractères que de lettres non accentuées 
            // et de chiffres, une erreur est générée
            if (!estAlphaNumerique($uneRepr->getId())) {
                GestionErreurs::ajouter("L'identifiant doit comporter uniquement des lettres non accentuées et des chiffres");
            } else {
                if (EtablissementDAO::isAnExistingId($unEtab->getId())) {
                    GestionErreurs::ajouter("La représentation " . $unEtab->getId() . " existe déjà");
                }
            }
        }
    }
    
    /*****************************************************************************
     * Méthodes permettant de préparer les informations à destination des vues
     ******************************************************************************/

    public function getTabRepresentations(): Array {
        $lesRepresentations = Array();
        $lesRepresentations = DaoRepresentation::getAll();
        
        return $lesRepresentations;
    }
}
