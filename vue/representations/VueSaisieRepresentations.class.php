<?php

namespace vue\representations;

use vue\VueGenerique;
use modele\metier\Representation;

/**
 * Description Page de saisie/modification d'une representation donnée
 * @author prof
 * @version 2018
 */
class VueSaisieRepresentations extends VueGenerique {

    /**
     * 
     * @var array 
     * liste des représentations 
     */
    private $lesRepresentations;

    /**
     * @var array 
     * liste des groupes 
     */
    private $lesGroupes;

    /**
     * @var array 
     * liste des lieux 
     */
    private $lesLieux;

    /**
     * @var array 
     * liste des date de représentations 
     */
    private $lesDates;

    /**
     * @var array 
     * liste des heures du debut de la représentation 
     */
    private $lesHeuresDebut;

    /**
     * @var array 
     * liste des heures de fin de la représentation 
     */
    private $lesHeuresFin;

    /**
     * @var Groupe 
     */
    private $unGroupe;

    private $uneRepresentation;

    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();
        ?>
        <form method="POST" action="index.php?controleur=representations&action=<?= $this->actionAEnvoyer ?>">
            <br>
            <table width="85%" cellspacing="0" cellpadding="0" class="tabNonQuadrille">

                <tr class="enTeteTabNonQuad">
                    <td colspan="3"><strong><?= $this->message ?></strong></td>
                </tr>
                
                <tr class="ligneTabNonQuad">
                    <td> Id*: </td>
                    <td><input type="text" value="<?= $this->uneRepresentation->getId() ?>" name="id" size ="20" 
                               maxlength="11"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Lieu*: </td>
                    <td><input type="text" value="<?= $this->uneRepresentation->getLeLieu()->getNomLieu() ?>" name="lieu" 
                               size="50" maxlength="45"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Groupe*: </td>
                    <td><input type="text" value="<?= $this->uneRepresentation->getLeGroupe()->getNom() ?>" name="groupe" size="50" maxlength="45"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Date*: </td>
                    <td><input type="text" value="<?= $this->uneRepresentation->getDateRep() ?>" name="date" size ="20" 
                               maxlength="11"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Heure Debut*: </td>
                    <td><input type="text" value="<?= $this->uneRepresentation->getHeureDebut() ?>" name="heuredebut" 
                               size="7" maxlength="5"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Heure Fin*: </td>
                    <td><input type="text" value="<?= $this->uneRepresentation->getHeureFin() ?>" name="heurefin" size="7" 
                               maxlength="5"></td>
                </tr>
                             
                </table>

            <table align="center" cellspacing="15" cellpadding="0">
                <tr>
                    <td align="right"><input type="submit" value="Valider" name="valider">
                    </td>
                    <td align="left"><input type="reset" value="Annuler" name="annuler">
                    </td>
                </tr>
            </table>
            <a href="index.php?controleur=representations&action=liste">Retour</a>
        </form>
        <?php
        include $this->getPied();
    }

    public function setUneRepresentation(Representation $uneRepresentation) {
        $this->uneRepresentation = $uneRepresentation;
    }


    public function setActionRecue(string $action) {
        $this->actionRecue = $action;
    }

    public function setActionAEnvoyer(string $action) {
        $this->actionAEnvoyer = $action;
    }

    public function setMessage(string $message) {
        $this->message = $message;
    }

}

