/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Mathias
 * Created: 6 oct. 2020
 */
CREATE TABLE `festival`.`Lieux` ( 
    `id_lieu` INT(3) NOT NULL , 
    `nom_lieu` VARCHAR(100) NOT NULL , 
    `adresse_lieu` VARCHAR(100) NOT NULL , 
    `capacite` INT(5) NOT NULL , 
    PRIMARY KEY (`id_lieu`)) ENGINE = InnoDB; 

