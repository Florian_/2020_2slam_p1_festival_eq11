<?php
namespace modele\dao;

use modele\metier\Representation;
use modele\metier\Groupe;
use modele\dao\DaoLieu;
use modele\dao\GroupeDAO;
use PDO;

/**
 * Description of DaoRepresentation
 * Classe métier :  Representation
 * @author eleve
 * @version 2020
 */
class DaoRepresentation {
    
    /**
     * Instancier un objet de la classe Representation à partir des tables Representation, Lieux, Groupe
     * @param array $enreg
     * @return Representation
     */
    protected static function enregVersMetier(array $enreg) {
        $id = $enreg['ID_REPRESENTATION'];
        $idGroupe = $enreg['ID_GROUPE'];
        $idLieu = $enreg['ID_LIEU'];
        $dateRep = $enreg['DATE'];
        $heureDebut = $enreg['HEURE_DEBUT'];
        $heureFin = $enreg['HEURE_FIN'];
       
        $objetLieu = DaoLieu::getOneById($idLieu);
        $objetGroupe = GroupeDAO::getOneById($idGroupe);
  
        $uneRepresentation = new Representation($id, $objetLieu , $objetGroupe, $dateRep, $heureDebut, $heureFin);
        return $uneRepresentation;
    }
    protected static function enregVersMetierDates(array $enreg) {
        $dates = $enreg['DATE'];       
        return ($dates);
    }
    
    /**
     * Retourne la liste de toutes les representations 
     * @return array tableau d'objets de representations
     */    
    public static function getAll() {
          $lesObjets = array();
          $requete = "SELECT * FROM Representation";
          $stmt = Bdd::getPdo()->prepare($requete);
          $ok = $stmt->execute();
          if ($ok) {

              while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {

                  $lesObjets[] = self::enregVersMetier($enreg);
              }
          }
          return $lesObjets;
      }
      public static function getAllDate() {
          $lesDates = array();
          $requete = "SELECT DATE FROM Representation";
          $stmt = Bdd::getPdo()->prepare($requete);
          $ok = $stmt->execute();
          if ($ok) {

              while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {

                  $lesDates[] = self::enregVersMetierDates($enreg);
              }
          }
          return $lesDates;
      }
          public static function getOneById($id) {
        $objetConstruit = null;
        $requete = "SELECT * FROM Representation WHERE ID_REPRESENTATION = :id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        // attention, $ok = true pour un select ne retournant aucune ligne
        if ($ok && $stmt->rowCount() > 0) {
            $objetConstruit = self::enregVersMetier($stmt->fetch(PDO::FETCH_ASSOC));
        }
        return $objetConstruit;
    }
    
    /**
     * Insérer un nouvel enregistrement dans la table à partir de l'état d'un objet métier
     * @param Etablissement $objet objet métier à insérer
     * @return boolean =FALSE si l'opération échoue
     */
    public static function insert(Representation $objet) {
        $requete = "INSERT INTO Representation VALUES (:id_representation, :id_groupe, :date, :heure_debut, :heure_fin)";
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnreg($objet, $stmt);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }

    /**
     * Mettre à jour enregistrement dans la table à partir de l'état d'un objet métier
     * @param string identifiant de l'enregistrement à mettre à jour
     * @param Etablissement $objet objet métier à mettre à jour
     * @return boolean =FALSE si l'opérationn échoue
     */
    public static function update($id, Representation $objet) {
        $ok = false;
        $requete = "UPDATE  Representation SET ID_REPRESENTATION=:id_representation, ID_GROUPE=:id_groupe,
           DATE=:date, HEURE_DEBUT=:heure_debut, HEURE_FIN=:heure_fin,
           WHERE ID_REPRESENTATION=:id";
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnreg($objet, $stmt);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }

     /**
     * Détruire un enregistrement de la table ETABLISSEMENT d'après son identifiant
     * @param string identifiant de l'enregistrement à détruire
     * @return boolean =TRUE si l'enregistrement est détruit, =FALSE si l'opération échoue
     */
    public static function delete($id) {
        $ok = false;
        $requete = "DELETE FROM Representation WHERE ID_REPRESENTATION = :id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        $ok = $ok && ($stmt->rowCount() > 0);
        return $ok;
    }
    
    /**
     * Permet de vérifier s'il existe ou non un établissement ayant déjà le même identifiant dans la BD
     * @param string $id identifiant de l'établissement à tester
     * @return boolean =true si l'id existe déjà, =false sinon
     */
    public static function isAnExistingId($id) {
        $requete = "SELECT COUNT(*) FROM Representation WHERE ID_REPRESENTATION=:id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        return $stmt->fetchColumn(0);
    }
    
}