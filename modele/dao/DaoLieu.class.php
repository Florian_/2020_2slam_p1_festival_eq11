<?php

namespace modele\dao;

use modele\metier\Lieu;
use PDO;

/**
 * Description of DaoLieu
 * Classe métier :  Lieu
 * @author eleve
 * @version 2020
 */
class DaoLieu {
    
    /**
     * Instancier un objet de la classe Lieu à partir d'un enregistrement de la table LIEUX
     * @param array $enreg
     * @return Lieu
     */
     protected static function enregVersMetier(array $enreg) {
        $id = $enreg['ID_LIEU'];
        $nom = $enreg['NOM_LIEU'];
        $adr = $enreg['ADRESSE_LIEU'];
        $CapAcceuil = $enreg['CAPACITE'];
        $objetMetier = new Lieu($id, $nom, $adr, $CapAcceuil);
        return $objetMetier;
    }
    
    /**
     * Retourne la liste de tous les lieux 
     * @return array tableau d'objets de lieux
     */
    public static function getAll() {
        $lesObjets = array();
        $requete = "SELECT * FROM Lieux";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }
    
    /**
     * Recherche un lieu selon la valeur de son identifiant
     * @param string $id
     * @return Lieu le lieu trouvé ; null sinon
     */
    public static function getOneById($id) {
        $objetConstruit = null;
        $requete = "SELECT * FROM Lieux WHERE ID_LIEU = :ID";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':ID', $id);
        $ok = $stmt->execute();
        // attention, $ok = true pour un select ne retournant aucune ligne
        if ($ok && $stmt->rowCount() > 0) {
            $objetConstruit = self::enregVersMetier($stmt->fetch(PDO::FETCH_ASSOC));
        }
        return $objetConstruit;
    }
    
}