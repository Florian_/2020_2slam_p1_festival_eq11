<?php
namespace modele\metier;

/**
 * Description of Lieu
 * un lieu du festival où se produisent un/des groupe(s)
 * @author eleve
 */
class Lieu {
    /**
     * identifiant du lieu ("xx")
     * @var string
     */
    private $id;
    /**
     * nom du lieu
     * @var string
     */
    private $nomlieu;
    /**
     * adresse du groupe
     * @var string
     */
    private $adresse;
    /**
     * capacité du lieu
     * @var integer
     */
    private $capacite;
    
    public function __construct($id, $nomlieu, $adresse, $capacite){
        $this->id = $id;
        $this->nomlieu = $nomlieu;
        $this->adresse = $adresse;
        $this->capacite = $capacite;
    }
    
    function getId() {
        return $this->id;
    }

    function getNomLieu() {
        return $this->nomlieu;
    }

    function getAdresse() {
        return $this->adresse;
    }

    function getCapacite() {
        return $this->capacite;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNomLieu($nomlieu) {
        $this->nomlieu = $nomlieu;
    }

    function setAdresse($adresse) {
        $this->adresse = $adresse;
    }

    function setCapacite($capacite) {
        $this->capacite = $capacite;
    }
    

}
